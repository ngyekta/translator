import 'dart:ui';

class AppColors {
  static const purple = Color(0xFF6973F4);
  static const blueishDarkGrey = Color(0xFF404350);
  static const grey = Color(0xFFD7D9E8);
}